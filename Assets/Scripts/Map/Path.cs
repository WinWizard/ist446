// File: Node.cs
// Author(s): Dylan Kozicki (dylan.kozicki@gmail.com)
// Description: This class is a map path
//              which handles various path related things
using UnityEngine;
using UnityEngine.UI;
using System;
[System.Serializable]
public class Path :  MonoBehaviour
{
    public bool isEnabled;
    MapLocation parent;
    //Function: Start
    //Author: Dylan Kozicki (dylan.kozicki@gmail.com)
    //Input: N/A
    //Output: N/A
    //Description: Runs at map open.
    public void Start(){
        parent = GetComponentInParent<MapLocation>();

        SetPathState(isEnabled);


    }


    //Function: SetPathState
    //Author: Dylan Kozicki (dylan.kozicki@gmail.com)
    //Input: state - the desired state of the path
    //Output: N/A
    //Description: Sets path state
    public void SetPathState(bool state){
            if(!parent.isEnabled && state)
                parent.isEnabled = true;
            GetComponent<Image>().enabled = state;
            GetComponent<BoxCollider2D>().enabled = state;
            isEnabled = state;

    }







}

using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class MapController : MonoBehaviour
{


    public MapGuiController controller;
    public bool isEnabled;
    private float oldScale;





    void Start()
    {
        oldScale = Time.timeScale;
        SetMapState(isEnabled);
    }
    public void SetMapState(bool state)
    {
        isEnabled = state;

        if (state)
        {
            oldScale = Time.timeScale;

            controller.gameObject.active = state;
            controller.WakeUp();
            Time.timeScale = 0;

            GameObject lvl = GameObject.FindWithTag("LevelObject");
            if (lvl)
            {
                DestroyImmediate(lvl);
            }
        }
        else
        {
            Time.timeScale = oldScale;
            controller.gameObject.active = state;

        }


    }
}

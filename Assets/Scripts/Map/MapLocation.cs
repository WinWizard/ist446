// File: MapLocation.cs
// Author(s): Dylan Kozicki (dylan.kozicki@gmail.com)
// Description: This class is a map MapLocation
//              which handles connected paths and such

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

[System.Serializable]

public class MapLocation :  MonoBehaviour
{

    public GameObject levelPrefab;
    public bool isEnabled;
    public bool isSelected;
    public GameObject selectedPrefab;
    private GameObject selector;
    ModalPanelDetails dialog;
    ModalPanel modalPanel;
    MapController mapController;


    public List<Path> paths = new List<Path>();

    //Function: Start
    //Author: Dylan Kozicki (dylan.kozicki@gmail.com)
    //Input: N/A
    //Output: N/A
    //Description: Runs at map open. Stores paths connected
    //             and sets their default state
    void Start(){
        SetSelect(isSelected);
        GetComponent<Button>().onClick.AddListener(OnClick);

        mapController =ItemController.Instance().GetComponentInParent<MapController>();



    }
    void OnClick(){

            CreateDialog();
        if(!modalPanel)
            modalPanel = ModalPanel.Instance();
        modalPanel.NewChoice(dialog);
    }
    void OnTriggerEnter2D(Collider2D collider){
        Debug.Log(collider.gameObject.tag);
        if(!isEnabled && collider.gameObject.tag == "Path"){
            isEnabled = true;
        }
    }

    public void SetSelect(bool selected){
        isSelected = selected;
        if(selected && !selector){
        selector = Instantiate(selectedPrefab, gameObject.transform.position, new Quaternion());
        selector.transform.parent = this.gameObject.transform;
        }
        else if(!selected && selector){
        DestroyImmediate(selector);
        }
    }
    void CreateDialog(){
        dialog =  new ModalPanelDetails ();
        if(isEnabled){
        dialog.question = "Would you like to warp to " + levelPrefab.name;
        dialog.button1Details = new EventButtonDetails ();
        dialog.button1Details.buttonTitle = "Ok";
        dialog.button1Details.action = LoadLevel;
        dialog.button2Details = new EventButtonDetails();

        dialog.button2Details.buttonTitle = "Cancel";
        dialog.button2Details.action = Cancel;
        }
        else{
            dialog.question = "Selected level is not yet unlocked.";
            dialog.button1Details = new EventButtonDetails ();
            dialog.button1Details.buttonTitle = "Ok";
            dialog.button1Details.action = Cancel;
        }
    }
    void LoadLevel(){

        Instantiate(levelPrefab, new Vector3(0,0,0), new Quaternion());
        mapController.controller.current.SetSelect(false);
        SetSelect(true);
        mapController.controller.current = this;
        mapController.SetMapState(false);

    }
    void Cancel(){

    }



}

// File: ForceField.cs
// Author(s): Dylan Kozicki (dylan.kozicki@gmail.com)
// Description: This controls the ForceField
//              should be attached to each Node.
using UnityEngine;
using System;
public class Door : MonoBehaviour
{

    public bool isOpen = true;
    public Portal portal;
    public GameObject[] lights = new GameObject[2];
    public float blinkRate = 1.5F;
    //Function: Start
    //Author: Dylan Kozicki (dylan.kozicki@gmail.com)
    //Input: N/A
    //Output: N/A
    //Description: Runs at map open.
    public void Start()
    {
        SetDoorState(isOpen);
    }

    public void SetDoorState(bool state)
    {
        isOpen = state;
        portal.gameObject.GetComponent<Renderer>().enabled = isOpen;
        portal.gameObject.GetComponent<BoxCollider2D>().enabled = isOpen;
        if (isOpen)
        {
            lights[0].GetComponent<Renderer>().material.SetColor("_Color", Color.green);
            lights[0].GetComponent<Renderer>().enabled = true;
            lights[1].GetComponent<Renderer>().material.SetColor("_Color", Color.green);
            lights[1].GetComponent<Renderer>().enabled = true;
            CancelInvoke("Blink");
        }
        else
        {

            lights[0].GetComponent<Renderer>().material.SetColor("_Color", Color.red);
            lights[1].GetComponent<Renderer>().material.SetColor("_Color", Color.red);
            CancelInvoke("Blink");

            InvokeRepeating("Blink", 1, blinkRate);
        }


    }
    private void Blink()
    {
        lights[0].GetComponent<Renderer>().enabled = !lights[0].GetComponent<Renderer>().enabled;
        lights[1].GetComponent<Renderer>().enabled = !lights[1].GetComponent<Renderer>().enabled;
    }


}

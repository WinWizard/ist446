// File: ForceField.cs
// Author(s): Dylan Kozicki (dylan.kozicki@gmail.com)
// Description: This controls the ForceField
//              should be attached to each Node.
using UnityEngine;
using System;
public class ForceFieldBeam :  MonoBehaviour
{
    public ForceField[] terminuses; //make sure terminus 0 is farther left/down from terminus 1
    public float scaleModifier = 3.66f;

    //Function: Start
    //Author: Dylan Kozicki (dylan.kozicki@gmail.com)
    //Input: N/A
    //Output: N/A
    //Description: Runs at map open.
    public void Start(){

        GetComponent<Renderer>().enabled = false;
        MakeBeam();
    }

    public bool MakeBeam(){
        if (terminuses[0] != null && terminuses[1] != null)
        {
            Vector3 lTerminus,rTerminus;
            if(terminuses[0].transform.position.x < terminuses[1].transform.position.x){
                rTerminus = terminuses[1].gameObject.transform.position;
                lTerminus = terminuses[0].gameObject.transform.position;

            }
            else{
                rTerminus = terminuses[0].gameObject.transform.position;
                lTerminus = terminuses[1].gameObject.transform.position;
            }

            if (Math.Floor(rTerminus.y*10) == Math.Floor(lTerminus.y*10))
            {

            Vector3 pos = new Vector3((terminuses[0].gameObject.transform.position.x+terminuses[1].transform.position.x)/2,
                                      terminuses[0].gameObject.transform.position.y,-100);
                this.gameObject.transform.localScale = new Vector3(1, Math.Abs(rTerminus.x - lTerminus.x) / scaleModifier, 1000);
                Debug.Log("LEN: " +Math.Abs(rTerminus.x - lTerminus.x) + "MOD" + scaleModifier+ "LEN/MOD: " + Math.Abs(rTerminus.x - lTerminus.x)/scaleModifier);
                this.gameObject.transform.position = pos;
                this.gameObject.transform.Rotate(Vector3.forward*45);
            }
        else if(Math.Floor(rTerminus.x*10) == Math.Floor(lTerminus.x*10)){
            Vector3 pos = new Vector3(terminuses[0].gameObject.transform.position.x,
                                      (terminuses[0].gameObject.transform.position.y + terminuses[1].transform.position.y) / 2, -100);
                                      Debug.Log("LEN: " +Math.Abs(rTerminus.y - lTerminus.y) + "LEN/MOD: " + Math.Abs(rTerminus.y - lTerminus.y)/scaleModifier);

                this.gameObject.transform.localScale = new Vector3(1,Math.Abs(rTerminus.y-lTerminus.y)/scaleModifier,1000);
                this.gameObject.transform.position = pos;
            }
            else{
                Vector3 pos = new Vector3((terminuses[0].gameObject.transform.position.x+terminuses[1].transform.position.x)/2,
                                          (terminuses[0].gameObject.transform.position.y+terminuses[1].transform.position.y)/2,-100);
            this.gameObject.transform.localScale = new Vector3(1,(float)Math.Sqrt(Math.Pow(rTerminus.x-lTerminus.x,2) +  Math.Pow(rTerminus.y-lTerminus.y,2)) / scaleModifier,1000);
            this.gameObject.transform.position = pos;

            this.gameObject.transform.LookAt(rTerminus, Vector3.left);

            }
            GetComponent<Renderer>().enabled = true;

            return true;
        }
        return false;
    }



}

// File: ForceField.cs
// Author(s): Dylan Kozicki (dylan.kozicki@gmail.com)
// Description: This controls the ForceField
//              should be attached to each Node.
using UnityEngine;
using System;
using System.Collections.Generic;
public class Item :  MonoBehaviour
{
    public enum ItemType{
        shot,
        other
    }
    public enum ItemTier{
        Common,
        Uncommon,
        Rare,
        Epic,
        Mythical
    }


    public ItemTier tier; //only need 1-5
    public Dictionary<Stats, float> modifiers = new Dictionary<Stats, float>();
    public Entry[] iStats;
    public ItemType type;
    public Projectile projectile;
    public bool isUnique;
    void Awake(){
        foreach(Entry ent in iStats ){
            modifiers.Add(ent.name, ent.entry);
        }
        if((int)type == (int)ItemType.shot && projectile){
            foreach(KeyValuePair<Stats,float> kvp in projectile.stats){
                if(modifiers.ContainsKey(kvp.Key)){
                    modifiers[kvp.Key] += kvp.Value;
                }else{
                    modifiers.Add(kvp.Key,kvp.Value);
                }
            }
        }
    }








}

// File){ ForceField.cs
// Author(s)){ Dylan Kozicki (dylan.kozicki@gmail.com)
// Description){ This controls the ForceField
//              should be attached to each Node.
using UnityEngine;
using System;
using System.Collections.Generic;
[System.Serializable]
public struct ItemPool
{
    public List<Item> common;
    public List<Item> uncommon;
    public List<Item> rare;
    public List<Item> epic;
    public List<Item> mythical;

};
public class ItemController : MonoBehaviour
{
    public ItemPool itemPool = new ItemPool();
    public static ItemController itemController;
    public static ItemController Instance()
    {
        if (!itemController)
        {
            itemController = Resources.FindObjectsOfTypeAll<ItemController>()[0];
            if (!itemController)
                Debug.LogError("There needs to be one active ItemController script on a GameObject in your scene.");
        }

        return itemController;
    }

    void Awake()
    {
        DontDestroyOnLoad(this);
    }
    public void SpawnRandomItem(Item.ItemTier minTier, Item.ItemTier maxTier, float ffdepth)
    {
        System.Random rand = new System.Random();
        int random = rand.Next((int)minTier, (int)maxTier);
        if (random == (int)Item.ItemTier.Common)
        {
            random = rand.Next(0, itemPool.common.Count - 1);
            Instantiate(itemPool.common[random], new Vector3(0f, 0f, ffdepth), new Quaternion());
        }
        else if (random == (int)Item.ItemTier.Uncommon)
        {
            random = rand.Next(0, itemPool.uncommon.Count - 1);
            Instantiate(itemPool.uncommon[random], new Vector3(0f, 0f, ffdepth), new Quaternion());
        }
        else if (random == (int)Item.ItemTier.Rare)
        {
            random = rand.Next(0, itemPool.rare.Count - 1);
            Instantiate(itemPool.rare[random], new Vector3(0f, 0f, ffdepth), new Quaternion());
        }
        else if (random == (int)Item.ItemTier.Epic)
        {
            random = rand.Next(0, itemPool.epic.Count - 1);
            Instantiate(itemPool.epic[random], new Vector3(0f, 0f, ffdepth), new Quaternion());
        }
        else if (random == (int)Item.ItemTier.Mythical)
        {
            random = rand.Next(0, itemPool.mythical.Count - 1);
            Instantiate(itemPool.mythical[random], new Vector3(0f, 0f, ffdepth), new Quaternion());
        }
    }
    public void RemoveItemFromPool(Item item)
    {
        if ((int)item.tier == (int)Item.ItemTier.Common)
        {
            itemPool.common.Remove(item);
        }
        else if ((int)item.tier == (int)Item.ItemTier.Uncommon)
        {
            itemPool.uncommon.Remove(item);
        }
        else if ((int)item.tier == (int)Item.ItemTier.Rare)
        {
            itemPool.rare.Remove(item);
        }
        else if ((int)item.tier == (int)Item.ItemTier.Epic)
        {
            itemPool.epic.Remove(item);
        }
        else if ((int)item.tier == (int)Item.ItemTier.Mythical)
        {
            itemPool.mythical.Remove(item);
        }

    }

}

// File: Node.cs
// Author(s): Dylan Kozicki (dylan.kozicki@gmail.com)
// Description: This class is a map path
//              which handles various path related things
using UnityEngine;
using System;
public class LevelGenerator :  MonoBehaviour
{
    public float difficulty;
    public int max_height;
    public Item.ItemTier item_mintier;
    public Item.ItemTier item_maxtier;
    public int min_height;
    ItemController itemController;

    public int height;
    public int ffdepth;
    public int max_width;
    public int min_width;
    public int width;
    public room_type rmtype;
    public ForceField[] forceFieldNodes;
    public ForceFieldBeam[] forceFieldBeams;
    public room_shape rmshape;
    public ForceField ffPrefab;
    public ForceFieldBeam beamPrefab;
    public Door doorPrefab;
    public Door[] doors = new Door[2];
    public bool entranceIsOpen;
    public GameObject player;

    //Function: Start
    //Author: Dylan Kozicki (dylan.kozicki@gmail.com)
    //Input: N/A
    //Output: N/A
    //Description: Runs at map open.
    public void Start()
    {
        GenerateForceField();
        player.transform.position = new Vector3(-((width / 2.0f) - 1.5f),0f,player.transform.position.z);
        itemController = ItemController.Instance();
        itemController.SpawnRandomItem(item_mintier, item_maxtier, ffdepth);


    }


    //Function: Start
    //Author: Dylan Kozicki (dylan.kozicki@gmail.com)
    //Input: N/A
    //Output: N/A
    //Description: creates the room boundary forcefield
    void GenerateForceField()
    {
        System.Random rand = new System.Random();

        if (height < min_height){
            height = rand.Next(min_height, max_height);
        }

            if (width < min_width){
                width = rand.Next(min_width, max_width);
            }
            switch(rmshape){
                case room_shape.Rectangle:
                        forceFieldNodes = new ForceField[4];
                        forceFieldBeams = new ForceFieldBeam[4];

                        //I removed the door at the entrance -Zach
                        //doors[0] = (Door)Instantiate(doorPrefab, new Vector3(-((width / 2.0f) - 0.5f), 0f, ffdepth), new Quaternion());
                        //doors[0].SetDoorState(entranceIsOpen);
                        //doors[0].portal.SetExit(false);
                        doors[1] = (Door)Instantiate(doorPrefab, new Vector3(((width / 2.0f)-0.5f),0f ,ffdepth), new Quaternion());
                        doors[1].SetDoorState(false);
                        doors[1].portal.SetExit(true);


                        forceFieldNodes[0] =(ForceField)Instantiate(ffPrefab, new Vector3(((width / 2.0f)+0.5f),-((height / 2.0f)+0.5f) ,ffdepth), new Quaternion());
                        forceFieldNodes[1] = (ForceField)Instantiate(ffPrefab, new Vector3( ((width / 2.0f)+0.5f),((height / 2.0f)+0.5f), ffdepth), new Quaternion());
                        forceFieldNodes[2] = (ForceField)Instantiate(ffPrefab, new Vector3( -((width / 2.0f)+0.5f),-((height / 2.0f)+0.5f), ffdepth), new Quaternion());
                        forceFieldNodes[3] = (ForceField)Instantiate(ffPrefab, new Vector3( -((width / 2.0f) + 0.5f), ((height / 2.0f) + 0.5f),ffdepth), new Quaternion());


                        forceFieldBeams[0] =(ForceFieldBeam)Instantiate(beamPrefab, new Vector3(), new Quaternion());
                        forceFieldBeams[0].terminuses = new ForceField[2];
                        forceFieldBeams[0].terminuses[0] = forceFieldNodes[3];
                        forceFieldBeams[0].terminuses[1] = forceFieldNodes[1];
                        forceFieldBeams[0].MakeBeam();

                        forceFieldBeams[1] =(ForceFieldBeam)Instantiate(beamPrefab, new Vector3(), new Quaternion());
                        forceFieldBeams[1].terminuses = new ForceField[2];
                        forceFieldBeams[1].terminuses[0] = forceFieldNodes[1];
                        forceFieldBeams[1].terminuses[1] = forceFieldNodes[0];
                        forceFieldBeams[1].MakeBeam();


                        forceFieldBeams[2] =(ForceFieldBeam)Instantiate(beamPrefab, new Vector3(), new Quaternion());
                        forceFieldBeams[2].terminuses = new ForceField[2];
                        forceFieldBeams[2].terminuses[0] = forceFieldNodes[0];
                        forceFieldBeams[2].terminuses[1] = forceFieldNodes[2];
                        forceFieldBeams[2].MakeBeam();


                        forceFieldBeams[3] =(ForceFieldBeam)Instantiate(beamPrefab, new Vector3(), new Quaternion());
                        forceFieldBeams[3].terminuses = new ForceField[2];
                        forceFieldBeams[3].terminuses[0] = forceFieldNodes[2];
                        forceFieldBeams[3].terminuses[1] = forceFieldNodes[3];
                        forceFieldBeams[3].MakeBeam();

                break;
            }
    }

    public enum room_type{
        Generic,
        Treasure,
        Boss,


    };
    public enum room_shape{
        Rectangle,
        Circle,

    };

}

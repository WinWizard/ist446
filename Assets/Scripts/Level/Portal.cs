// File: ForceField.cs
// Author(s): Dylan Kozicki (dylan.kozicki@gmail.com)
// Description: This controls the ForceField
//              should be attached to each Node.
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
public class Portal :  MonoBehaviour
{

    bool isExit = false;
    ModalPanelDetails dialog;
    ModalPanel modalPanel;
    MapController mapController;
    //Function: Start
    //Author: Dylan Kozicki (dylan.kozicki@gmail.com)
    //Input: N/A
    //Output: N/A
    //Description: Runs at map open.
    public void Start(){
        mapController =ItemController.Instance().GetComponentInParent<MapController>();
        CreateDialog();

        modalPanel = ModalPanel.Instance();

    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.gameObject.tag == "Player"){
        if(isExit){
                //TODO add next level code here
            }
            if(!modalPanel){
                    modalPanel = ModalPanel.Instance();
            }
            if(dialog == null){
                CreateDialog();
            }
            modalPanel.NewChoice (dialog);

        }
    }
    void LoadMap(){

        mapController.SetMapState(true);

    }

    public void SetExit(bool state){
        isExit = state;
    }

    void Cancel()
    {

    }
    void CreateDialog(){
        dialog =  new ModalPanelDetails ();
        dialog.question = "Would you like to return to the world map?";
        dialog.button1Details = new EventButtonDetails ();
        dialog.button1Details.buttonTitle = "Ok";
        dialog.button1Details.action = LoadMap;
        dialog.button2Details = new EventButtonDetails();

        dialog.button2Details.buttonTitle = "Cancel";
        dialog.button2Details.action = Cancel;
    }

}

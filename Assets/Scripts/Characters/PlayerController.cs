// File: Node.cs
// Author(s): Dylan Kozicki (dylan.kozicki@gmail.com)
// Description: This class is a map path
//              which handles various path related things
using UnityEngine;
using System;
using System.Collections.Generic;

public enum Stats
{
    speed,
    dmg,
    shieldRegen,
    hullHealth,
    fireRate,
    range,
    luck,
    lives
}
[System.Serializable]
public struct Entry
{
    public Stats name;
    public float entry;
}
public class PlayerController : MonoBehaviour
{

    //Player Stats

    protected Dictionary<Stats, float> stats = new Dictionary<Stats, float>();
    public Entry[] pStats = new Entry[]
    {
        new Entry(){name=Stats.speed, entry=2.0f},
        new Entry(){name=Stats.dmg, entry=1.0f},
        new Entry(){name=Stats.shieldRegen, entry=1.0f},
        new Entry(){name=Stats.hullHealth, entry=1.0f},
        new Entry(){name=Stats.fireRate, entry=1.0f},
        new Entry(){name=Stats.luck, entry=1.0f},
        new Entry(){name=Stats.range, entry=1.0f},
        new Entry(){name=Stats.lives, entry=1.0f}
    };
    protected Dictionary<Stats, float> modifiers = new Dictionary<Stats, float>();
    //Internal Logic
    private float xMove;
    private float yMove;
    protected float myTime;
    protected float nextFire;

    private Rigidbody2D playerBody;
    Vector2 movement;
    public List<Item> items = new List<Item>();
    public Item shot;
    ModalPanel modalPanel;
    ItemController itemController;
    bool moving = false;

    //Needed globals for player health
    public float hullHealth=10;     //this is the player's current hullHealth
    public float shieldHealth;   //this is the players current shieldHealth

    protected bool isPlayer; //quick and dirty method of checting if the current class is PlayerController or GenericEnemy

    //Function: Start
    //Author: Dylan Kozicki (dylan.kozicki@gmail.com)
    //Input: N/A
    //Output: N/A
    //Description: Runs at map open.
    public void Start()
    {
        foreach (Entry ent in pStats)
        {
            stats.Add(ent.name, ent.entry);
        }
        playerBody = GetComponent<Rigidbody2D>();
        modalPanel = ModalPanel.Instance();
        itemController = ItemController.Instance();
        foreach (Item item in items)
        {
            ApplyModifiers(item.modifiers);
        }
        ApplyModifiers(shot.modifiers);

        //initalize the player's mutable health values
        hullHealth = GetModifiedValue(Stats.hullHealth);
        shieldHealth = 1;   //for now shield max will stay at 1 forever because I cant be asked to implement it right now

        //initialize that this is the player
        isPlayer = true;
    }

    public void AddItem(Item item)
    {
        if (item.type == Item.ItemType.shot)
        {
            shot = item;
        }
        else
        {

            items.Add(item);
        }
        ApplyModifiers(item.modifiers);
        DontDestroyOnLoad(item);
        if (!item.isUnique)
        {
            itemController.RemoveItemFromPool(item);
        }
        Destroy(item.gameObject);
    }
    protected void ApplyModifiers(Dictionary<Stats, float> mods)
    {

        foreach (KeyValuePair<Stats, float> kvp in mods)
        {
            if (modifiers.ContainsKey(kvp.Key))
            {

                modifiers[kvp.Key] += kvp.Value;
            }
            else
            {
                modifiers.Add(kvp.Key, kvp.Value);
            }
        }




    }
    protected void RemoveModifiers(Dictionary<Stats, float> mods)
    {
        foreach (KeyValuePair<Stats, float> kvp in mods)
        {
            if (modifiers.ContainsKey(kvp.Key))
            {
                modifiers[kvp.Key] -= kvp.Value;
            }

        }

    }
    public void DoDamage(GameObject src, Projectile proj)
    {
        float damageDealt = GetModifiedValue(Stats.dmg);

        //BEGIN DEALING DAMAGE

        //check if the player has shields
        if(shieldHealth > 0)
        {
            //check if player has enough shields to tank the hit
            if(damageDealt <= shieldHealth)
            {
                shieldHealth -= damageDealt;
            }
            //if the damage is more than the shields can take then split the damage to shields and hull
            else
            {
                hullHealth -= (damageDealt - shieldHealth); //hull takes any damage the shields dont absorb
                shieldHealth = 0;   //because it's split damage we know the shields are gone
            }
        }
        //otherwise the player doesnt have shields and takes all hull damage
        else
        {
            hullHealth -= damageDealt;
        }

        //END DEALING DAMAGE

        //CHECK IF PLAYER IS ALIVE
        if(hullHealth < 1)
        {
            if(GetModifiedValue(Stats.lives) > 1)//Respawn if the player has lives left. For now respawn will give them full health and shield and kick them to stage select
            {
                //do respawn
            }
            //if no lives remain then kill the player
            else
            {
                //check if its a player
                if (isPlayer)
                {

                }
                //otherwise its an enemy
                else
                {
                    Destroy(this.gameObject);
                } 
               
            }
        }
        //END CHECK IF PLAYER IS ALIVE
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Item")
        {
            Item item = other.gameObject.GetComponent<Item>();
            ModalPanelDetails dialog = new ModalPanelDetails();
            dialog.question = "Would you like to pick up " + item.gameObject.name + "?";
            foreach (KeyValuePair<Stats, float> kvp in item.modifiers)
            {
                dialog.question += "\n" + Enum.GetName(typeof(Stats), kvp.Key) + ": " + (kvp.Value >= 0 ? "+ " : "") + kvp.Value;

            }
            dialog.iconImage = item.gameObject.GetComponent<SpriteRenderer>().sprite;
            dialog.iconColor = item.gameObject.GetComponent<SpriteRenderer>().color;
            dialog.button1Details = new EventButtonDetails();
            dialog.button1Details.buttonTitle = "Yes";
            dialog.button1Details.action = () => { AddItem(item); };
            dialog.button2Details = new EventButtonDetails();

            dialog.button2Details.buttonTitle = "No";
            dialog.button2Details.action = CancelItem;
            modalPanel.NewChoice(dialog);

        }
    }

    void CancelItem()
    {

    }
    void Update()
    {
        moving = false;
        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            xMove = Input.GetAxis("Horizontal") * GetModifiedValue(Stats.speed);
            yMove = Input.GetAxis("Vertical") * GetModifiedValue(Stats.speed);

            movement = new Vector2(xMove, yMove);
            moving = true;
        }



    }
    protected void FireProjectile(Vector3 direction)
    {
        Projectile s = (Projectile)Instantiate(shot.projectile, this.gameObject.transform.position, new Quaternion());
        Physics2D.IgnoreCollision(s.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        s.FireProjectile(this.gameObject, direction);
    }
    void FixedUpdate()
    {
        myTime += Time.deltaTime;

        if (moving)
            playerBody.AddForce(movement);
        if (myTime > nextFire && Input.GetButton("FireUp"))
        {
            nextFire = myTime + GetModifiedValue(Stats.fireRate);
            FireProjectile(new Vector3(0, 0, -90));
        }
        if (myTime > nextFire && Input.GetButton("FireDown"))
        {
            nextFire = myTime + GetModifiedValue(Stats.fireRate);

            FireProjectile(new Vector3(0, 0, 90));
        }
        if (myTime > nextFire && Input.GetButton("FireLeft"))
        {
            nextFire = myTime + GetModifiedValue(Stats.fireRate);
            FireProjectile(new Vector3(0, 0, 0));

        }
        if (myTime > nextFire && Input.GetButton("FireRight"))
        {
            nextFire = myTime + GetModifiedValue(Stats.fireRate);

            FireProjectile(new Vector3(0, 0, 180));
        }

    }

    protected float GetModifiedValue(Stats stat)
    {
        if (modifiers.ContainsKey(stat))
        {
            return stats[stat] + modifiers[stat];
        }
        return stats[stat];
    }
}

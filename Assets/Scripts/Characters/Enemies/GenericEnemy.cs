// File: GenericEnemy.cs
// Author(s): Dylan Kozicki (dylan.kozicki@gmail.com)
// Description: This is the base eneymy class.


using UnityEngine;
using System.Collections.Generic;

public class GenericEnemy : PlayerController
{
    EnemyController controller;
    public EnemyList.EnemyTier tier;




    //Function: Start
    //Author: Dylan Kozicki (dylan.kozicki@gmail.com)
    //Input: N/A
    //Output: N/A
    //Description: Runs at map open. Stores paths connected
    //             and sets their default state



    void Start()
    {
        controller = EnemyController.Instance();

        if (controller)
        {
            Debug.Log("C Found");
        }
        else
        {
            Debug.Log("C Not Found");
        }
        foreach (Entry ent in pStats)
        {
            stats.Add(ent.name, ent.entry);
        }
        foreach (Item item in items)
        {
            ApplyModifiers(item.modifiers);
        }
        ApplyModifiers(shot.modifiers);


        //initialize that this is an enemy
        isPlayer = false;

    }
    void OnTriggerEnter2D(Collider2D other)
    {

    }
    void FixedUpdate()
    {
        myTime += Time.deltaTime;

        if (myTime > nextFire)
        {
            nextFire = myTime + GetModifiedValue(Stats.fireRate);
            //TODO fix hacky fire code.
            Quaternion tmp = Quaternion.LookRotation(controller.player.transform.position - transform.position);
            tmp.x = 0;
            tmp.y = 0;
            Vector3 rot = tmp.eulerAngles;

            if (controller.player.transform.position.x > 0)
            {
                rot.Set(rot.x, rot.y, rot.z + 180); ;
            }
            FireProjectile(rot);
        }
    }

    void update()
    {

    }

}

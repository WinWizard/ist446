// File: EnemyController.cd
// Author(s): Dylan Kozicki (dylan.kozicki@gmail.com)
// Description: This class controls overall enemy behavior
//              Including random generation of enemies.
using UnityEngine;
using System;
using System.Collections.Generic;
[System.Serializable]
public struct EnemyList
{
    public List<GenericEnemy> weakling;
    public List<GenericEnemy> easy;
    public List<GenericEnemy> medium;
    public List<GenericEnemy> hard;
    public List<GenericEnemy> extreme;
    public List<GenericEnemy> boss;
    public enum EnemyTier
    {
        weakling,
        easy,
        medium,
        hard,
        extreme,
        boss
    };
    public List<GenericEnemy> GetFromIndex(EnemyTier enemy)
    {

        switch (enemy)
        {
            case EnemyTier.weakling:
                {
                    return weakling;
                }
            case EnemyTier.easy:
                {
                    return easy;
                }
            case EnemyTier.medium:
                {
                    return medium;
                }
            case EnemyTier.hard:
                {
                    return hard;
                }
            case EnemyTier.extreme:
                {
                    return extreme;
                }
            case EnemyTier.boss:
                {
                    return boss;
                }

        }
        return null;
    }

}
public class EnemyController : MonoBehaviour
{
    System.Random rand = new System.Random();


    public EnemyList enemyPool = new EnemyList();
    public List<GenericEnemy> enemies = new List<GenericEnemy>();
    public int max_enemies;
    public int min_enemies;
    public EnemyList.EnemyTier min_enemy_tier;
    public EnemyList.EnemyTier max_enemy_tier;
    public GameObject player;
    public static EnemyController enemyController;
    public static EnemyController Instance()
    {
        if (!enemyController)
        {
            enemyController = Resources.FindObjectsOfTypeAll<EnemyController>()[0];
            if (!enemyController)
                Debug.LogError("There needs to be one active enemyController script on a GameObject in your scene.");
        }

        return enemyController;
    }


    //Function: Start
    //Author: Dylan Kozicki (dylan.kozicki@gmail.com)
    //Input: N/A
    //Output: N/A
    //Description: Runs at map open. Stores paths connected
    //             and sets their default state
    void Start()
    {

        enemies.AddRange(GameObject.FindObjectsOfType<GenericEnemy>());
        int enemyCount;
        int eTier;
        int eIndex;
        if (enemies.Count < max_enemies)
        {
            if (enemies.Count > min_enemies)
                enemyCount = rand.Next(enemies.Count, max_enemies);
            else
                enemyCount = rand.Next(min_enemies, max_enemies);
            while (enemies.Count < enemyCount)
            {
                List<GenericEnemy> temp = enemyPool.GetFromIndex((EnemyList.EnemyTier)rand.Next((int)min_enemy_tier, (int)max_enemy_tier));
                enemies.Add((GenericEnemy)Instantiate(temp[rand.Next(0, temp.Count - 1)], RandomLoc(), new Quaternion()));
            }

        }



    }

    Vector3 RandomLoc()
    {
        //TODO randomize location
        return new Vector3(0, 0, 0);
    }
}

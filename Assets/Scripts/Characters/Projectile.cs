// File: ForceField.cs
// Author(s): Dylan Kozicki (dylan.kozicki@gmail.com)
// Description: This controls the ForceField
//              should be attached to each Node.
using UnityEngine;
using System;
using System.Collections.Generic;
public class Projectile : MonoBehaviour
{
    public enum PathType
    {
        linear,
        spread,
        homing,
    };
    public enum Damage
    {
        explosion,
        hit,
        aoe
    };

    public PathType pathType;
    public Dictionary<Stats, float> stats = new Dictionary<Stats, float>();
    public Entry[] pStats;
    private float ttl;
    GameObject source; //might be redundant
    GameObject target;
    Rigidbody2D rb;
    void Awake()
    {
        foreach (Entry ent in pStats)
        {
            stats.Add(ent.name, ent.entry);
        }
        rb = GetComponent<Rigidbody2D>();
    }
    void Start()
    {

    }
    void FixedUpdate()
    {
        ttl += Time.deltaTime;
        if (ttl <= stats[Stats.range])
        {
            if ((int)pathType == (int)PathType.homing)
            {

            }
            //    else if((int)pathType == (int)PathType.linear)
        }
        else
        {
            DestroyImmediate(this.gameObject);
        }
    }


    public void FireProjectile(GameObject src, Vector3 fireDirection)
    {
        source = src;

        if ((int)pathType == (int)PathType.homing)
        {

            rb.WakeUp();
        }
        else if ((int)pathType == (int)PathType.linear)
        {

            this.gameObject.transform.Rotate(fireDirection);
            this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, 8);
            rb.freezeRotation = true;
            rb.WakeUp();

            GetComponent<Renderer>().enabled = true;

            rb.AddRelativeForce(Vector2.left * stats[Stats.speed], ForceMode2D.Impulse);
        }



    }
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag != source.tag)
        {
            if (other.gameObject.tag == "Enemy")
            {
                DoProjectileDamage((other.gameObject.GetComponent<GenericEnemy>()));
            }
            else if (other.gameObject.tag == "Player")
            {
                DoProjectileDamage(other.gameObject.GetComponent<PlayerController>());

            }
            Destroy(this.gameObject);
        }
    }

    void DoProjectileDamage(PlayerController target)
    {
        target.DoDamage(source, this);


    }








}
